﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Runtime.Serialization;
using System.IO;
using Newtonsoft.Json;
using IcimsRestConnector.Entities;


namespace WeatherConnector
{
    class WeatherClient
    {
        WeatherFilter wf=new WeatherFilter();
        internal const string GetUri = "http://api.openweathermap.org/data/2.5/weather?q={0},Ind&appid={1}";

        public Entity.WeatherInfo GetWeatherApi(string appid,WeatherFilter wf)
        {
            WebRequest wrGETURL;
            var value = wf.value[0];
            
            var uri = string.Format(GetUri,value,appid);
            wrGETURL = WebRequest.Create(uri);
            
            wrGETURL.Method = "GET";
            wrGETURL.ContentType = "application/json";
            WebResponse resp = wrGETURL.GetResponse();
            Stream objStream;
            objStream = resp.GetResponseStream();
            StreamReader objReader;
            objReader = new StreamReader(objStream);
            string json = objReader.ReadToEnd();
            Entity.WeatherInfo wc = JsonConvert.DeserializeObject<Entity.WeatherInfo>(json);
            return wc;
           }

              
    }
}
