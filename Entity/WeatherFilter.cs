﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcimsRestConnector.Entities
{
    public class WeatherFilter
    {
        public string name { get; set; }
        public List<string> value { get; set; }
        public string @operator { get; set; }
    }
}

