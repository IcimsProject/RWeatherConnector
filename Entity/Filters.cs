﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scribe.Core.ConnectorApi.Query;
using IcimsRestConnector.Entities;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Exceptions;

namespace WeatherConnector.Entity
{
    class Filters
    {
        public static WeatherFilter CreateWeatherFilter(Query query)
        {
            WeatherFilter WeatherFilter = new WeatherFilter();
            if (query.Constraints != null)
            {
                var comparisonExpression = query.Constraints as ComparisonExpression;
                if (comparisonExpression.Operator == ComparisonOperator.Equal)
                {
                    WeatherFilter.@operator = "=";
                }
                else if (comparisonExpression.Operator == ComparisonOperator.Greater)
                {
                    WeatherFilter.@operator = ">";
                }
                else if (comparisonExpression.Operator == ComparisonOperator.Less)
                {
                    WeatherFilter.@operator = "<";
                }
                else
                {
                    WeatherFilter.@operator = "";
                }
                WeatherFilter.name = comparisonExpression.LeftValue.Value.ToString();
                string rightHandValue = comparisonExpression.RightValue.Value.ToString();
                
                WeatherFilter.value = new List<string>() { rightHandValue };

            }
            return WeatherFilter;
        }

       
        }
       
    
}
