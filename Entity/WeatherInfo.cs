﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherConnector.Entity
{
      public class WeatherInfo
        {
          public int visibility { get; set; }
          public int dt { get; set; }
          public int id { get; set; }
          public string name { get; set; }
          public int cod { get; set; }
        }

}
