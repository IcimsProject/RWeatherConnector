﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherConnector
{
    class ConnectorSettings
    {
       
        public const string ConnectorTypeId = "{DFBD9879-E046-4D39-9114-233EAB45ACCB}";// For testing purpose, Comment this while pushing to production, If not already commented.
        public const string ConnectorVersion = "1.0";
        public const string Description = "Provided by HRNX LLC";
        public const string Name = "WeatherTest";
        public const bool SupportsCloud = false;


    }
}
