﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WeatherConnector.Entity;

namespace WeatherConnector
{
    class MetadataProvoider:IMetadataProvider
    {
        #region Member Properties

        /// <summary>
        /// Contains the collection of Entity types. Used to generate Metadata for Scribe Online.
        /// </summary>
        private Dictionary<string, Type> EntityCollection = new Dictionary<string, Type>();

        private const string WeatherEntity = "WeatherInfo";
        #endregion
     
        public const string QueryActionName="Query";

        public MetadataProvoider()
        {

            //Fill the Entity collection. We'll use this to create metadata when Scribe Online asks for it:
            EntityCollection = PopulateEntityCollection();

        }
        public void ResetMetadata()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            var actionDefinitions = new List<IActionDefinition>();
            var queryDef = new ActionDefinition
            {
                SupportsConstraints = true,
                SupportsRelations = false,
                SupportsLookupConditions = false,
                SupportsSequences = false,
                KnownActionType = KnownActions.Query,
                SupportsBulk = false,
                Name = KnownActions.Query.ToString(),
                FullName = KnownActions.Query.ToString(),
                Description = string.Empty
            };

            actionDefinitions.Add(queryDef);

            return actionDefinitions;
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            IObjectDefinition objectDefinition = null;

            if (EntityCollection.Count > 0)
            {
                foreach (var keyValuePair in EntityCollection)
                {
                    if (keyValuePair.Key == objectName)
                    {
                        Type entityType = keyValuePair.Value;
                        if (entityType != null)
                        {
                            //hand the type down to our reflection method and create an IObjectDefiniton for Scribe Online
                            objectDefinition = GetObjectDefinition(entityType, true);
                        }
                    }
                }
            }

            return objectDefinition;
        }

        private IObjectDefinition GetObjectDefinition(Type entityType, bool shouldGetFields)
        {

            IObjectDefinition objectDefinition = null;

            objectDefinition = new ObjectDefinition
            {
                Name = entityType.Name,
                FullName = entityType.Name,
                Description = string.Empty,
                Hidden = false,
                RelationshipDefinitions = new List<IRelationshipDefinition>(),
                PropertyDefinitions = new List<IPropertyDefinition>(),
                SupportedActionFullNames = new List<string>()
            };

            objectDefinition.SupportedActionFullNames.Add("Query");

            if (entityType.Name == WeatherEntity)
            {

                objectDefinition.SupportedActionFullNames.Add("Create");

            }

            if (shouldGetFields)
            {
                objectDefinition.PropertyDefinitions = GetFieldDefinitions(entityType);
            }
            return objectDefinition;

        }

        private List<IPropertyDefinition> GetFieldDefinitions(Type entityType)
        {

            var fields = new List<IPropertyDefinition>();

            //Pull a collection from the incoming entity:
            var fieldsFromType = entityType.GetProperties(BindingFlags.Instance | BindingFlags.FlattenHierarchy |
                BindingFlags.Public | BindingFlags.GetProperty);

            foreach (var field in fieldsFromType)
            {

                var propertyDefinition = new PropertyDefinition
                {
                    Name = field.Name,
                    FullName = field.Name,
                    PropertyType = field.PropertyType.ToString(),
                    PresentationType = field.PropertyType.ToString(),
                    Nullable = false,
                    IsPrimaryKey = false,
                    UsedInQueryConstraint = true,
                    UsedInQuerySelect = true,
                    UsedInActionOutput = true,
                    UsedInQuerySequence = true,
                    Description = field.Name,
                };

                //Find any of the following fields that may be attached to the entity. 
                //These are defined by the attributes attached to the property
                foreach (var attribute in field.GetCustomAttributes(false))
                {

                    //whether the field is readonly or not
                    if (attribute is ReadOnlyAttribute)
                    {
                        var readOnly = (ReadOnlyAttribute)attribute;
                        propertyDefinition.UsedInActionInput = readOnly == null || !readOnly.IsReadOnly;
                    }

                    //whether the field is required to be populated on an insert
                    if (attribute is RequiredAttribute)
                    {
                        propertyDefinition.RequiredInActionInput = true;
                    }

                    //if the field can be used as a match field for the query
                    if (attribute is KeyAttribute)
                    {
                        propertyDefinition.UsedInLookupCondition = true;
                    }

                }

                fields.Add(propertyDefinition);

            }

            return fields;

        }


        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            foreach (var entityType in EntityCollection)
            {
                yield return RetrieveObjectDefinition(entityType.Key, shouldGetProperties, shouldGetRelations);
            }
        }

        private Dictionary<string, Type> PopulateEntityCollection()
        {
            Dictionary<string, Type> entities = new Dictionary<string, Type>();

            entities.Add(WeatherEntity, typeof(WeatherInfo));

            return entities;

        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
