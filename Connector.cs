﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.ConnectionUI;
using System.Collections.ObjectModel;
using Scribe.Core.ConnectorApi.Query;
using System.ComponentModel.Composition;
using System.Reflection;
using WeatherConnector.Entity;
//Direct Change In Original Repository

namespace WeatherConnector
{
    [ScribeConnector(
       ConnectorSettings.ConnectorTypeId,
       ConnectorSettings.Name,
       ConnectorSettings.Description,
       typeof(Connector),
       StandardConnectorSettings.SettingsUITypeName,
       StandardConnectorSettings.SettingsUIVersion,
       StandardConnectorSettings.ConnectionUITypeName,
       StandardConnectorSettings.ConnectionUIVersion,
       StandardConnectorSettings.XapFileName,
       new[] { "Scribe.IS.Source", "Scribe.IS.Target", "Scribe.IS2.Source", "Scribe.IS2.Target" },
       ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]

    public class Connector:IConnector
    { 
        private string appid ;
       // internal IDictionary<string, string> _connectionInfo = new Dictionary<string, string>();
        WeatherClient _weatherClient = new WeatherClient();
        public void Connect(IDictionary<string, string> properties)
        {
          this.appid=properties["appid"];
           this.IsConnected = true;
        }

       

        public Guid ConnectorTypeId
        {
            get { throw new NotImplementedException(); }
        }

        public void Disconnect()
        { 

            this.IsConnected = false;
        }

        public Scribe.Core.ConnectorApi.Actions.MethodResult ExecuteMethod(Scribe.Core.ConnectorApi.Actions.MethodInput input)
        {
            throw new NotImplementedException();
        }

        public Scribe.Core.ConnectorApi.Actions.OperationResult ExecuteOperation(Scribe.Core.ConnectorApi.Actions.OperationInput input)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {

            IEnumerable<DataEntity> results = new List<DataEntity>();

            string entityName = query.RootEntity.ObjectDefinitionFullName;

            //Route the request to the correct 'Get' in the REST class.
            //Then convert our entity object into a Scribe Online, generic entity
           

            switch (entityName)
            {
                case "WeatherInfo":
                    var weather = _weatherClient.GetWeatherApi(appid,Filters.CreateWeatherFilter(query));
                    List<WeatherInfo> listweather = new List<WeatherInfo>();
                    listweather.Add(weather);
                    results = this.ToDataEntity<WeatherInfo>(listweather);
                    break;

                default:
                    break;
            }

            return results;



        }

        private IEnumerable<DataEntity> ToDataEntity<T>(IEnumerable<T> entities)
        {
            //get the type and its properties. We'll use this to build the fields with Reflection
            var type = typeof(T);
            var fields = type.GetProperties(BindingFlags.Instance |
                BindingFlags.FlattenHierarchy |
                BindingFlags.Public |
                BindingFlags.GetProperty);

            //Loop through the retrieved entities and create a DataEntity from it: 
            foreach (var entity in entities)
            {

                var dataEntity = new QueryDataEntity
                {
                    ObjectDefinitionFullName = type.Name,
                    Name = type.Name
                };

                //Add the fields to the entity:

                /* Each KeyValuePair in the fields must be complete.
                 * If the field's value is NULL here, Scribe OnLine will throw
                 * an exception. 
                 * To send a NULL field to Scribe Online, just don't add it to this dictionary.
                 */

                foreach (var field in fields)
                {
                    dataEntity.Properties.Add(
                        field.Name,
                        field.GetValue(entity, null));
                }

                //Hand back the completed object: 
                yield return dataEntity.ToDataEntity();
            }

        }
        private MetadataProvoider metadataprovider = new MetadataProvoider();
        public IMetadataProvider GetMetadataProvider()
        {
            return this.metadataprovider;
        }

        public bool IsConnected
        {
            get;
            set;
        }

        public string PreConnect(IDictionary<string, string> properties)
        {
            var form = new FormDefinition
            {
                CompanyName = "HRNXLLC",
                CryptoKey ="1",
                HelpUri = new Uri("http://icimshub.force.com/customer/s/"),
                Entries =
                new Collection <EntryDefinition>
               {
                   new EntryDefinition
                   {
                       InputType=InputType.Text,
                        IsRequired=true,
                        Label="App Id:",
                        PropertyName="appid"
                   }
               }
            };
            return form.Serialize();
        }//preConnect closed....

    }
}
